package com.example.mvianimationsample

import io.reactivex.Observable

interface SideEffect<Action, State> {

    fun bind(actions: Observable<Action>, state: State): Observable<out Action>

}