package com.example.mvianimationsample.mainactivity

import com.example.mvianimationsample.Reducer
import com.example.mvianimationsample.mainactivity.MainActivityAction.*
import com.example.mvianimationsample.mainactivity.MainActivityState.*

object MainActivityReducer :
    Reducer<MainActivityAction, MainActivityState> {

    override fun reduce(action: MainActivityAction, state: MainActivityState): MainActivityState {
        return when (action) {
            AnimationStartAction ->
                state.copy(text = "Animation started", animationState = AnimationState.START, isButtonEnabled = false)
            AnimationEndAction ->
                state.copy(text = "Animation ended", animationState = AnimationState.END, isButtonEnabled = true)
            ButtonClickAction -> state.copy(text = "button clicked")
        }
    }
}