package com.example.mvianimationsample.mainactivity

import com.example.mvianimationsample.MViViewModel

class MainActivityViewModel : MViViewModel<MainActivityAction, MainActivityState>(MainActivityStore) {
}