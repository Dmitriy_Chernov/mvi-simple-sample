package com.example.mvianimationsample.mainactivity

import com.example.mvianimationsample.Store

object MainActivityStore: Store<MainActivityAction, MainActivityState>(
    reducer = MainActivityReducer,
    sideEffects = listOf(AnimationSideEffect),
    initialState = MainActivityState.INITIAL_STATE
)