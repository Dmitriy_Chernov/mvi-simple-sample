package com.example.mvianimationsample.mainactivity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProviders
import com.example.mvianimationsample.MviView
import com.example.mvianimationsample.R
import com.example.mvianimationsample.mainactivity.MainActivityAction.*
import com.example.mvianimationsample.mainactivity.MainActivityState.AnimationState.*
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MviView<MainActivityAction, MainActivityState> {


    private val actionsRelay = PublishRelay.create<MainActivityAction>()

    override val actions: Observable<MainActivityAction> = actionsRelay


    override fun render(state: MainActivityState) {
        with(state) {
            textView.text = text
            floatingActionButton.isEnabled = isButtonEnabled

            when (animationState) {
                START -> {
                    val animation = AnimationUtils.loadAnimation(this@MainActivity, R.anim.rotate)
                        .apply {
                            setAnimationListener(object : Animation.AnimationListener {
                                override fun onAnimationRepeat(animation: Animation?) {
                                }

                                override fun onAnimationEnd(animation: Animation?) {
                                    actionsRelay.accept(AnimationEndAction)
                                }

                                override fun onAnimationStart(animation: Animation?) {
                                }

                            })
                        }

                    textView.startAnimation(animation)
                }
                END -> textView.clearAnimation()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java).bind(this)

        floatingActionButton.setOnClickListener { actionsRelay.accept(ButtonClickAction) }
    }

    override fun onDestroy() {
        super.onDestroy()
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java).unbind()
    }
}
