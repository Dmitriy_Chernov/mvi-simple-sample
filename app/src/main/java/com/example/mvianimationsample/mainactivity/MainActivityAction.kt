package com.example.mvianimationsample.mainactivity

sealed class MainActivityAction {

    object ButtonClickAction: MainActivityAction()
    object AnimationStartAction: MainActivityAction()
    object AnimationEndAction: MainActivityAction()

}