package com.example.mvianimationsample.mainactivity

import com.example.mvianimationsample.SideEffect
import com.example.mvianimationsample.mainactivity.MainActivityAction.*
import io.reactivex.Observable
import io.reactivex.rxkotlin.ofType
import java.util.concurrent.TimeUnit

object AnimationSideEffect : SideEffect<MainActivityAction, MainActivityState> {

    override fun bind(
        actions: Observable<MainActivityAction>,
        state: MainActivityState
    ): Observable<out MainActivityAction> {
        return actions.ofType<ButtonClickAction>()
            .map { AnimationStartAction }
    }

}