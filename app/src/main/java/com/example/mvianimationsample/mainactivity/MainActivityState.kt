package com.example.mvianimationsample.mainactivity

data class MainActivityState(
    val text: String?,
    val isButtonEnabled: Boolean,
    val animationState: AnimationState
) {

    enum class AnimationState { START, END }

    companion object {
        val INITIAL_STATE = MainActivityState(
            text = "initial state",
            isButtonEnabled = true,
            animationState = AnimationState.END
        )
    }
}