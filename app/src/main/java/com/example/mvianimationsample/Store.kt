package com.example.mvianimationsample

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.withLatestFrom

open class Store<Action, State>(
    private val reducer: Reducer<Action, State>,
    private val sideEffects: List<SideEffect<Action, State>>,
    private val initialState: State
) {

    private val internalActions = PublishRelay.create<Action>()
    private val currentState = BehaviorRelay.createDefault(initialState)

    fun wire(): Disposable {
        val disposable = CompositeDisposable()

        disposable += internalActions
            .withLatestFrom(currentState) { action, state -> reducer.reduce(action, state) }
            .distinctUntilChanged()
            .subscribe(currentState::accept)

        disposable += Observable.fromIterable(sideEffects)
            .flatMap {
                it.bind(internalActions, currentState.value ?: initialState)
                    .withLatestFrom(currentState) { action, state -> reducer.reduce(action, state) }
            }
            .subscribe(currentState::accept)

        return disposable
    }

    fun bind(view: MviView<Action, State>): Disposable {
        val disposable = CompositeDisposable()

        disposable += currentState.observeOn(AndroidSchedulers.mainThread()).subscribe(view::render)
        disposable += view.actions.subscribe(internalActions::accept)

        return disposable
    }

}