package com.example.mvianimationsample

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.Disposable

open class MViViewModel<Action, State>(private val store: Store<Action, State>) : ViewModel() {

    private val wiring = store.wire()
    private var viewBinding: Disposable? = null

    override fun onCleared() {
        super.onCleared()
        wiring.dispose()
    }

    fun bind(view: MviView<Action, State>) {
        viewBinding = store.bind(view)
    }

    fun unbind() {
        viewBinding?.dispose()
    }

}