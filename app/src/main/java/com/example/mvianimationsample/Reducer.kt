package com.example.mvianimationsample

interface Reducer<Action, State> {

    fun reduce(action: Action, state: State): State

}